package icp.test;

import geom.Vect3d;
import icp.ICP;
import icp.ICPException;
import icp.Surface3D;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by kiril on 22.06.2017.
 */

public class ICPTest {
    public static ArrayList<Vect3d> loadPointsFromFile(String filePath) throws FileNotFoundException {
        System.out.println("Load data from: " + filePath);

        FileInputStream fis = new FileInputStream(filePath);
        Scanner sc = new Scanner(fis);
        sc.useLocale(Locale.US);

        ArrayList<Vect3d> points = new ArrayList<>();

        while (sc.hasNext()) {
            Vect3d point = new Vect3d(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
            points.add(point);
        }

        sc.close();
        System.out.println("Loaded");

        return points;
    }

    public static void main(String[] args) {

        try {
            ArrayList<Vect3d> data1 = loadPointsFromFile("1.txt");
            ArrayList<Vect3d> data2 = loadPointsFromFile("2.txt");

            Surface3D source = new Surface3D(data1);
            Surface3D target = new Surface3D(data2);

            ICP icp = new ICP(source, target);
            System.out.println(String.format("RMSE: %s", icp.calculate()));
        } catch (ICPException ex) {
            System.err.println(ex);
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
        }
    }
}
