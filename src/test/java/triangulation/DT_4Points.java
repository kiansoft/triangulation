package triangulation;

import geom.Vect3d;
import org.junit.Assert;
import org.junit.Test;
import triangulation.algorithms.DCA_Modify;
import triangulation.structures.Node;

import java.util.ArrayList;

public class DT_4Points {
    @Test
    public void degenerate4Equal() throws Exception {
        Vect3d n = Vect3d.O;
        DCA_Modify dca = new DCA_Modify(n, n, n, n);
        dca.process();
        System.out.println(dca);
    }

    @Test
    public void normal_2Tr_right() throws Exception {
        Vect3d o = Vect3d.O;
        Vect3d a = new Vect3d(4, 1, 0);
        Vect3d b = new Vect3d(5, 3, 0);
        Vect3d c = new Vect3d(2, 3, 0);

        DCA_Modify dca = new DCA_Modify(o, a, b, c);
        dca.process();
        ArrayList tr = dca.getTriangulation();
        System.out.println(dca);
    }

    @Test
    public void normal_3Tr_right() throws Exception {
        Vect3d o = new Vect3d(4, 2, 0);
        Vect3d a = new Vect3d(4, 1, 0);
        Vect3d b = new Vect3d(5, 3, 0);
        Vect3d c = new Vect3d(2, 3, 0);

        DCA_Modify dca = new DCA_Modify(o, a, b, c);
        dca.process();
        ArrayList<Node> tr = dca.getTriangulation();

        for (Node n : tr) {
            Assert.assertEquals(n.get_countNeighbors(), 3);
        }

        System.out.println(dca);
    }
}
