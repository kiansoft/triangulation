package triangulation;

import geom.Vect3d;
import org.junit.Assert;
import org.junit.Test;
import triangulation.algorithms.OrientationChecker;

public class OrinentationTest {
    @Test
    public void rightOrientation() {
        Vect3d a = new Vect3d(4, 1, 0);
        Vect3d b = new Vect3d(5, 3, 0);
        Vect3d c = new Vect3d(2, 3, 0);

        Assert.assertEquals(OrientationChecker.isRightOrientation2D(a, b, c), true);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(b, c, a), true);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(c, a, b), true);
    }

    @Test
    public void leftOrientation() {
        Vect3d a = new Vect3d(4, 1, 0);
        Vect3d b = new Vect3d(5, 3, 0);
        Vect3d c = new Vect3d(2, 3, 0);

        Assert.assertEquals(OrientationChecker.isRightOrientation2D(a, c, b), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(b, a, c), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(c, b, a), false);
    }

    @Test
    public void degenerate() {
        System.out.println("Run degenerate test");
        Vect3d a = new Vect3d(4, 1, 0);
        Vect3d b = new Vect3d(4, 1, 0);
        Vect3d c = new Vect3d(4, 1, 0);

        Assert.assertEquals(OrientationChecker.isRightOrientation2D(a, c, b), false);

        Vect3d o = Vect3d.O;
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, o, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(a, o, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(b, o, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(c, o, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, a, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, b, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, c, o), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, o, a), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, o, b), false);
        Assert.assertEquals(OrientationChecker.isRightOrientation2D(o, o, c), false);
    }
}
