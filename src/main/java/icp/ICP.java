package icp;

import geom.Checker;
import geom.Vect3d;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;

/**
 * Created by kiril on 19.06.2017.
 */

public class ICP {
    private Surface3D source;
    private Surface3D target;

    private SingularValueDecomposition svd;

    private int threshold = 10;

    public ICP(Surface3D source, Surface3D target) {
        this.source = source;
        this.target = target;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public double calculate() throws ICPException {
        System.out.println("Start ICP...");
        this.swapSurface();
        return this.ICP_SVD();
    }

    private double ICP_SVD() {
        double error = 0;
        int N = this.source.countPoints();

        for (int iteration = 0; iteration < this.threshold; iteration++) {
            Vect3d source_centroid = this.source.getCenterOfMass();
            Vect3d target_centroid = this.target.getCenterOfMass();

            System.out.println("Source centroid: " + source_centroid);
            System.out.println("Target centroid: " + target_centroid);


            double[][] source_data = this.makeTrMatrix(this.source, source_centroid);
            double[][] target_data;
            if (this.source.countPoints() == this.target.countPoints()) {
                target_data = this.makeTrMatrix(this.target, target_centroid);
            } else {
                target_data = this.matchTargetPoints(target_centroid);
            }

            RealMatrix source_matrix = MatrixUtils.createRealMatrix(source_data);
            RealMatrix target_matrix = MatrixUtils.createRealMatrix(target_data).transpose();

            svd = new SingularValueDecomposition(source_matrix.multiply(target_matrix));

            RealMatrix U = svd.getU();
            RealMatrix V = svd.getV();
            RealMatrix S = svd.getS();

            RealMatrix rotate_matrix = V.multiply(U.transpose());

            // check reflection
            double det = new LUDecomposition(rotate_matrix).getDeterminant();

            if (det < 0) {
                System.out.println("Reflection detected");
                rotate_matrix.setColumnVector(2, rotate_matrix.getColumnVector(2).mapMultiply(-1));
            }

            System.out.println("Rotation: " + rotate_matrix);

            RealMatrix source_centroid_t = MatrixUtils.createRealMatrix(3, 1);
            source_centroid_t.setColumn(0, source_centroid.getArray());

            Vect3d rotated_source_centroid = this.arrayToVect(rotate_matrix.scalarMultiply(-1).multiply(source_centroid_t).getColumn(0));

            Vect3d translation = rotated_source_centroid.add(target_centroid);

            System.out.println("Translation: " + translation);

            this.source.rotate(rotate_matrix);
            this.source.move(translation);

            System.out.println(source);

            error = this.getError();

            if (error < Checker.eps()) {
                break;
            } else {
                System.out.println(String.format("Iteration: %s \nCurrent RMSE: %s", iteration + 1, error));
            }
        }

        return error;
    }

    private double getError() {
        int N = this.source.countPoints();
        double error = 0;
        for (int i = 0; i < N; i++) {
            Vect3d s = this.source.getPoint(i);
            Vect3d t;
            if (this.source.countPoints() == this.target.countPoints()) {
                t = this.target.getPoint(i);
            } else {
                t = this.target.findNearest(s);
            }

            error += s.sub(t).mag();
        }
        error = Math.sqrt(error / (double) N);
        return error;
    }

    private void swapSurface() {
        int target_count = this.target.countPoints();
        int source_count = this.source.countPoints();

        if (target_count < source_count) {
            Surface3D tmp = this.target;
            this.target = this.source;
            this.source = tmp;
        }
    }

    private double[][] matchTargetPoints(Vect3d centroid) {
        int N = this.source.countPoints();
        double[][] matched = new double[3][N];
        for (int i = 0; i < N; i++) {
            Vect3d s = this.source.getPoint(i);
            Vect3d t = this.target.findNearest(s).sub(centroid);

            matched[0][i] = t.x();
            matched[1][i] = t.y();
            matched[2][i] = t.z();
        }

        return matched;
    }

    private Vect3d arrayToVect(double[] array) {
        return new Vect3d(array[0], array[1], array[2]);
    }

    private double[][] makeTrMatrix(Surface3D surface, Vect3d centroid) {
        int N = surface.countPoints();

        double[][] data = new double[3][N];

        for (int i = 0; i < N; i++) {
            Vect3d point = surface.getPoint(i).sub(centroid);

            data[0][i] = point.x();
            data[1][i] = point.y();
            data[2][i] = point.z();
        }

        return data;
    }
}
