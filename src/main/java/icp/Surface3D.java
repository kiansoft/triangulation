package icp;

import geom.ExDegeneration;
import geom.Vect3d;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by kiril on 19.06.2017.
 */

public class Surface3D {
    private RealMatrix matrix;
    private int N;

    public Surface3D(ArrayList<Vect3d> points) {
        this.N = points.size();

        this.matrix = MatrixUtils.createRealMatrix(3, this.N);
        for (int i = 0; i < this.N; i++) {
            Vect3d p = points.get(i);
            this.matrix.setColumn(i, p.getArray());
        }
    }

    public Surface3D(final Vect3d... points) {
        this(new ArrayList<>(Arrays.asList(points)));
    }

    public Surface3D(final double[][] matrix) throws ExDegeneration {
        int dimension = matrix.length;
        if (dimension != 3) {
            throw new ExDegeneration("Cannot create Surface3D");
        }

        this.N = matrix[0].length;
        this.matrix = MatrixUtils.createRealMatrix(matrix);
    }

    /*
    Перемещает поверхность на заданный вектор
     */
    public void move(Vect3d direction) {
        RealMatrix dir_matr = MatrixUtils.createRealMatrix(3, this.N);
        double[] column = direction.getArray();
        for (int i = 0; i < this.N; i++) {
            dir_matr.setColumn(i, column);
        }

        this.matrix = this.matrix.add(dir_matr);
    }

    /*
    Поворачивает поверхность
     */
    public void rotate(RealMatrix matrix3x3) {
        this.matrix = matrix3x3.multiply(this.matrix);
    }

    /*
    Ищет ближайшую точку поверхности относительно заданной
     */
    public Vect3d findNearest(Vect3d point) {
        Vect3d resultPoint = this.getPoint(0);
        double minDist = Vect3d.dist(point, resultPoint);

        for (int i = 1; i < this.N; i++) {
            Vect3d tmpPoint = this.getPoint(i);
            double dist = Vect3d.dist(point, tmpPoint);

            if (dist < minDist) {
                minDist = dist;
                resultPoint = tmpPoint;
            }
        }

        return resultPoint;
    }

    public Vect3d getPoint(int index) throws IndexOutOfBoundsException {
        if (index < this.N) {
            Vect3d point = new Vect3d(this.matrix.getEntry(0, index), this.matrix.getEntry(1, index),
                    this.matrix.getEntry(2, index));
            return point;
        }

        throw new IndexOutOfBoundsException();
    }

    public int countPoints() {
        return this.N;
    }

    /*
    Возвращает точку центра масс поверхности
     */
    public Vect3d getCenterOfMass() {
        Vect3d center = new Vect3d();

        int count = this.countPoints();
        for (int i = 0; i < count; i++) {
            Vect3d point = this.getPoint(i);
            center = center.add(point);
        }

        return center.mul(1. / (double) (this.countPoints()));
    }

    public String toString() {
        String str = "Surface of " + this.N + " points";
        for (int i = 0; i < this.N; i++) {
            str += "\n" + this.getPoint(i).toString();
        }

        return str;
    }
}
