package triangulation.algorithms;

/**
 * Created by kiril on 24.09.2017.
 */

import geom.Checker;
import geom.ExDegeneration;
import geom.Vect3d;
import triangulation.structures.Circle;
import triangulation.structures.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Devide and conquer algorithm, author: Mestetskiy L.
 * Алгоритм построения трингуляции Делоне на плоскости.
 * 3я координата нужна только для хранения значения высоты точки однолистной поверхности
 */

public class DCA_Modify {
    public enum STATE {
        NOT_STARTED,
        PROCESSING,
        FINISHED
    }

    private enum DELAUNAY_CHECK_STATE {
        TRUE,
        FALSE,
        UNDEFINED
    }

    private class NewStarter {
        public Circle circle;
        public Node node;
    }

    private ArrayList<Vect3d> transformNodes(final Node... nodes) {
        ArrayList<Vect3d> points = new ArrayList<>(nodes.length);
        for (Node n : nodes) {
            points.add(n.get_point());
        }

        return points;
    }

    private boolean isValidTriangle(final Node n1, final Node n2, final Node n3) {
        ArrayList<Vect3d> points = this.transformNodes(n1, n2, n3);

        if (Checker.threePointsOnTheLine(points.get(0), points.get(1), points.get(2))) {
            return false;
        }

        try {
            if (!Checker.isConvexPolygon(points)) {
                return false;
            }
        } catch (ExDegeneration ex) {
            return false;
        }

        return true;
    }


    private ArrayList<Vect3d> _points;
    private ArrayList<Node> _triangulation;
    private STATE _state;

    public DCA_Modify(final Vect3d... points) {
        this(new ArrayList<>(Arrays.asList(points)));
    }

    public DCA_Modify(final ArrayList<Vect3d> points) {
        assert (points.size() > 2);
        this._points = points;

        this._state = STATE.NOT_STARTED;
    }

    public boolean process() throws Exception {
        this._state = STATE.PROCESSING;

        this.createNodes();

        this.recursiveTriangulationAlg();
        this._state = STATE.FINISHED;

        return true;
    }

    private void recursiveTriangulationAlg() throws Exception {
        int triangulationSize = this._triangulation.size();
        if (triangulationSize == 3) {
            this.makeTriangle(this._triangulation);
        } else if (triangulationSize == 4) {
            this.makeTrianglesOf4Nodes(this._triangulation);
        } else if (triangulationSize == 8) {

        } else if (triangulationSize < 12) {

        } else {
            // triangulationSize >= 12
        }
    }

    private void makeTriangle(ArrayList<Node> triangulation) throws Exception {
        assert triangulation.size() == 3;

        Node n1 = triangulation.get(0);
        Node n2 = triangulation.get(1);
        Node n3 = triangulation.get(2);

        if (this.isValidTriangle(n1, n2, n3)) {
            n1.addNeighbors(n2, n3);
            n2.addNeighbors(n1, n3);
            n3.addNeighbors(n1, n2);
        } else {
            throw new Exception("Degenerate triangle!");
        }
    }

    private void makeTrianglesOf4Nodes(ArrayList<Node> triangulation) {
        assert triangulation.size() == 4;

        Node n0 = triangulation.get(0);
        Node n1 = triangulation.get(1);
        Node n2 = triangulation.get(2);
        Node n3 = triangulation.get(3);

        if (isDelaunayTriangles(n0, n1, n2, n3) || isDelaunayTriangles(n0, n3, n2, n1)) {
            n0.addNeighbors(n1, n3);
            n1.addNeighbors(n0, n2, n3);
            n2.addNeighbors(n1, n3);
            n3.addNeighbors(n0, n1, n2);
        }
        if (isDelaunayTriangles(n1, n2, n3, n0) || isDelaunayTriangles(n1, n0, n3, n2)) {
            n0.addNeighbors(n1, n2, n3);
            n1.addNeighbors(n0, n2);
            n2.addNeighbors(n0, n1, n3);
            n3.addNeighbors(n0, n2);
        }
        if (isDelaunayTriangles(n2, n3, n0, n1) || isDelaunayTriangles(n2, n1, n0, n3)) {
            n0.addNeighbors(n1, n3);
            n1.addNeighbors(n0, n2, n3);
            n2.addNeighbors(n1, n3);
            n3.addNeighbors(n0, n1, n2);
        }
        if (isDelaunayTriangles(n3, n0, n1, n2) || isDelaunayTriangles(n3, n2, n1, n0)) {
            n0.addNeighbors(n1, n2, n3);
            n1.addNeighbors(n0, n2);
            n2.addNeighbors(n0, n1, n3);
            n3.addNeighbors(n0, n2);
        }
        if (isDelaunayTriangles(n1, n0, n2, n3) || isDelaunayTriangles(n1, n3, n2, n0)) {
            n0.addNeighbors(n1, n2, n3);
            n1.addNeighbors(n0, n3);
            n2.addNeighbors(n0, n3);
            n3.addNeighbors(n0, n1, n2);
        }
    }

    private boolean isRightOrientation(final Node n1, final Node n2, final Node n3) {
        return OrientationChecker.isRightOrientation2D(this.transformNodes(n1, n2, n3));
    }

    // !!! треугольник образуют точки (0, 1, 3) и (1, 2, 3)
    private boolean isDelaunayTriangles(Node n0, Node n1, Node n2, Node n3) {
        if (this.isValidTriangle(n0, n1, n3) && this.isValidTriangle(n1, n2, n3)) {
            if (this.isRightOrientation(n0, n1, n3) && this.isRightOrientation(n1, n2, n3)) {
                if (this.isOnDiferentSides(n0, n1, n2, n3)) {
                    return checkStateDelaunay(n0, n1, n2, n3);
                }
            }
        }

        return false;
    }

    private int sideValue(final Node n0, final Node n1, final Node n2) {
        return (int) (n0.x() * (n2.y() - n1.y()) + n1.x() * (n1.y() - n2.y()) -
                n0.y() * (n2.x() - n1.x()) - n1.y() * (n1.x() - n2.x()));
    }

    // !!! треугольник образуют точки (0, 1, 3) и (1, 2, 3)
    private boolean isOnDiferentSides(final Node n0, final Node n1, final Node n2, final Node n3) {
        int value_n0 = this.sideValue(n0, n1, n3);
        int value_n2 = this.sideValue(n2, n1, n3);

        if (value_n0 > 0 && value_n2 > 0) {
            return false;
        }
        if (value_n0 < 0 && value_n2 < 0) {
            return false;
        }

        return true;
    }

    // !!! треугольник образуют точки (0, 1, 3) и (1, 2, 3)
    private boolean checkStateDelaunay(Node n0, Node n1, Node n2, Node n3) {
        DELAUNAY_CHECK_STATE fasterCheckResult = this.fasterCheck(n0, n1, n2, n3);
        switch (fasterCheckResult) {
            case TRUE:
                return true;
            case FALSE:
                return false;
            default:
                return this.slowCheck(n0, n1, n2, n3);
        }
    }

    private DELAUNAY_CHECK_STATE fasterCheck(Node n0, Node n1, Node n2, Node n3) {
        double s_alpha = (n0.x() - n1.x()) * (n0.x() - n3.x()) + (n0.y() - n1.y()) * (n0.y() - n3.y());
        double s_beta = (n2.x() - n1.x()) * (n2.x() - n3.x()) + (n2.y() - n1.y()) * (n2.y() - n3.y());

        if ((s_alpha < 0) && (s_beta < 0)) {
            return DELAUNAY_CHECK_STATE.FALSE;
        }

        if (s_alpha >= 0 && s_beta >= 0) {
            return DELAUNAY_CHECK_STATE.TRUE;
        }

        return DELAUNAY_CHECK_STATE.UNDEFINED;
    }

    private boolean slowCheck(Node n0, Node n1, Node n2, Node n3) {

        double check =
                ((n0.x() - n1.x()) * (n0.y() - n3.y()) - (n0.x() - n3.x()) * (n0.y() - n1.y())) *
                        ((n2.x() - n1.x()) * (n2.x() - n3.x()) + (n2.y() - n1.y()) * (n2.y() - n3.y())) +
                        ((n0.x() - n1.x()) * (n0.x() - n3.x()) + (n0.y() - n1.y()) * (n0.y() - n3.y())) *
                                (-(n2.x() - n1.x()) * (n2.y() - n3.y()) + (n2.x() - n3.x()) * (n2.y() - n1.y()));

        if (check >= 0) return true;

        return false;
    }

    private void createNodes() {
        this._triangulation = new ArrayList<>(this._points.size());
        for (Vect3d point : this._points) {
            this._triangulation.add(new Node(point));
        }
    }

    public boolean isFinished() {
        return (this._state == STATE.FINISHED);
    }

    public ArrayList<Node> getTriangulation() {
        if (this._state == STATE.FINISHED) {
            return this._triangulation;
        }

        return new ArrayList<Node>();
    }

    public ArrayList<Node> mergeTriangulations(ArrayList<Node> tr1, ArrayList<Node> tr2) throws Exception {
        this.sortNodes(tr1);
        this.sortNodes(tr2);

        if (this.needSwapTriangulations(tr1, tr2)) {
            this.swapTriangulations(tr1, tr2);
        }

        // стартер находится во второй триангуляции
        Node starter = tr2.get(0);

        // находим узлы левее стартера из tr1
        ArrayList<Node> nodes = this.getNodesAtLeft(starter, tr1);

        NewStarter starterStruct = new NewStarter();

        // для каждого узла строим минимальную пустую окружность с центром на оси, параллельной oX
        // находим среди них минимальную по радиусу
        for (Node node : nodes) {
            double x0 = (node.x() * node.x() - starter.x() * starter.x() +
                    (starter.y() - node.y()) * (starter.y() - node.y())) / (2 * (starter.x() - node.x()));
            Vect3d center = new Vect3d(x0, starter.y(), starter.z());
            double radius = (x0 - starter.x()) * (x0 - starter.x());
            Circle circle = new Circle(center, radius);

            if ((starterStruct.node == null) ||
                    (!circle.containsAnyOf(tr1)) && radius < starterStruct.circle.radius()) {
                starterStruct.node = node;
                starterStruct.circle = circle;
            }
        }

        //строим ребро, объединяющее триангуляции
        starterStruct.node.addNeighbor(starter);
        starter.addNeighbor(starterStruct.node);

        return new ArrayList<>();
    }

    private ArrayList<Node> getNodesAtLeft(Node node, ArrayList<Node> tr) {
        ArrayList<Node> nodes = new ArrayList<>();
        for (Node n : tr) {
            if (this.compareNodes(n, node) != -1) {
                nodes.add(n);
            }
        }

        return nodes;
    }

    public void swapTriangulations(ArrayList<Node> tr1, ArrayList<Node> tr2) {
        ArrayList<Node> tmp;
        tmp = tr1;
        tr1 = tr2;
        tr2 = tmp;
    }

    private boolean needSwapTriangulations(ArrayList<Node> tr1, ArrayList<Node> tr2) {
        Node n1 = tr1.get(0);
        Node n2 = tr2.get(0);

        return this.compareNodes(n1, n2) == 1;
    }

    public Node findFirstStarter(ArrayList<Node> tr1, ArrayList<Node> tr2) throws Exception {
        Node n1 = tr1.get(0);
        Node n2 = tr2.get(0);

        switch (this.compareNodes(n1, n2)) {
            case 1: {
                return n2;
            }
            case 0: {
                return n2;
            }
            default:
                throw new Exception("Triangulations should be swaped!");
        }
    }

    public static int compareNodes(Node o1, Node o2) {
        if (o1.x() < o2.x()) {
            return 1;
        }

        if (o1.x() == o2.x()) {
            if (o1.y() < o2.y()) {
                return 1;
            }

            if (o1.y() == o2.y()) {
                return 0;
            }

            return -1;
        }

        return -1;
    }

    public void sortNodes(ArrayList<Node> nodes) {
        Collections.sort(nodes, new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return DCA_Modify.compareNodes(o1, o2);
            }
        });
    }

    @Override
    public String toString() {
        String str = "Algorithm state: " + this._state + '\n';
        if (this.isFinished()) {
            for (Node n : this._triangulation) {
                str += n.toString() + '\n';
            }
        }

        return str;
    }
}
