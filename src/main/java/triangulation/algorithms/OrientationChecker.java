package triangulation.algorithms;

import geom.Vect3d;

import java.util.ArrayList;

public class OrientationChecker {

    /**
     * Check triangle orientation, RIGHT orientation used by default
     *
     * @return true if orientation of tr is RIGHT
     */
    public static boolean isRightOrientation2D(Vect3d v1, Vect3d v2, Vect3d v3) {
        return counter_clockwise(v1.x(), v1.y(), v2.x(), v2.y(), v3.x(), v3.y());
    }

    public static boolean isRightOrientation2D(ArrayList<Vect3d> points) {
        return OrientationChecker.isRightOrientation2D(points.get(0), points.get(1), points.get(2));
    }

    /**
     * Return true if orientation is counter clockwise
     */
    private static boolean counter_clockwise(double x1, double y1, double x2, double y2, double x3, double y3) {
        return triangle_area_2(x1, y1, x2, y2, x3, y3) > 0.0;
    }

    /**
     * Return oriented area of triangle
     */
    private static double triangle_area_2(double x1, double y1, double x2, double y2, double x3, double y3) {
        return (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1);
    }
}
