package triangulation.structures;

/**
 * Created by kiril on 23.09.2017.
 */

/*
Node with _neighbors
 */

import geom.Vect3d;

import java.util.ArrayList;

public class Node {
    private Vect3d _point;
    private int _countNeighbors = 0;
    private ArrayList<Node> _neighbors;

    public Node(Vect3d point) {
        this._point = point;
        this._neighbors = new ArrayList<>();
    }

    public void addNeighbor(Node neighbor) {
        if (!this._neighbors.contains(neighbor)) {
            this._neighbors.add(neighbor);
            this._countNeighbors++;
        }
    }

    public void addNeighbors(final Node... nodes) {
        for (Node node : nodes) {
            this.addNeighbor(node);
        }
    }

    public double x() {
        return this._point.x();
    }

    public double y() {
        return this._point.y();
    }

    public double z() {
        return this._point.z();
    }

    public Vect3d get_point() {
        return _point;
    }

    public ArrayList<Vect3d> get_neighbors_points() {
        ArrayList<Vect3d> neighbors = new ArrayList<Vect3d>(this._countNeighbors);
        for (Node node : this._neighbors) {
            neighbors.add(node.get_point());
        }

        return neighbors;
    }

    public int get_countNeighbors() {
        return this._countNeighbors;
    }

    @Override
    public String toString() {
        return "Node{" +
                "_point=" + _point +
                ", _countNeighbors=" + _countNeighbors +
                ", _neighbors=" + this.get_neighbors_points() +
                '}';
    }
}
