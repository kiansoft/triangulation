package triangulation.structures;

import geom.Vect3d;

import java.util.ArrayList;

public class Circle {
    private Vect3d _center;
    private double _radius;
    private double _radius_2;

    private final double EPS = 0.01;

    public Circle(Vect3d center, double radius) {
        this._center = center;
        this._radius = radius;

        this._radius_2 = this._radius * this._radius;
    }

    public Vect3d center() {
        return this._center;
    }

    public double radius() {
        return this._radius;
    }

    // содержится ли какая-либо точка внутри окружности
    public boolean containsAnyOf(ArrayList<Node> nodes) {
        // TODO перебирать не все точки
        for (Node node : nodes) {
            if (Math.pow(node.x() - this._center.x(), 2) +
                    Math.pow(node.y() - this._center.y(), 2) <= this._radius_2) {
                return true;
            }
        }
        return false;
    }
}
