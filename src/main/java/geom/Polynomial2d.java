package geom;

//import static config.Config.LOG_LEVEL;

import java.util.ArrayList;
//import util.Log;

/**
 * Polynomial
 *
 * @author alena
 */
public class Polynomial2d implements i_Geom {

    private double[] _coefficients;

    public double[] coefficients() {
        return _coefficients;
    }

    /**
     * Constructor of polynomial
     */
    public Polynomial2d(double[] coefficients) throws ExDegeneration {
        _coefficients = coefficients;
    }

    /**
     * return point of the polynomial depending on the coefficients and degree
     *
     * @param param параметр по которому строится точка [-infinity, +infinity]
     * @return точку полинома
     */
    public Vect3d getPoint(double param) {
        double polynomialValue = 0;
        for (int i = 0; i < this._coefficients.length; i++) {
            if (this._coefficients[i] != 0)
                polynomialValue += this._coefficients[i] * Math.pow(param, i);
        }
        Vect3d p = new Vect3d(param, polynomialValue, 0);
        return p;
    }

    @Override
    public ArrayList<Vect3d> deconstr() {
        ArrayList<Vect3d> result = new ArrayList<>();
        /// TODO check не восстанавливается по точкам
        return result;
    }

    @Override
    public i_Geom constr(ArrayList<Vect3d> points) {
        /// TODO check не восстанавливается по точкам
        return null;
    }

    @Override
    public boolean isOrientable() {
        return false;
    }

    @Override
    public GeomType type() {
        return GeomType.POLYNOMIAL2D;
    }
}
